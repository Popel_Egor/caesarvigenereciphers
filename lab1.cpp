#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

void CaesarCode(char* input, char* output, char* shift);
void CaesarDecode(char* input, char* output, char* shift);
void VigenereCode(char* input, char* output, char* key_filename);
void VigenereDecode(char* input, char* output, char* key_filename);

int main (int argc, char **argv)
{
	char *input = NULL;
	char *output = NULL;
	char *shift = NULL;
	char *key = NULL;
	bool cflag = false, dflag = false, vflag = false;
	int c;
	opterr = 0;
	while ((c = getopt (argc, argv, "cdvi:o:s:k:")) != -1)
		switch (c)
		{
		case 'i':
			input = optarg;
			break;
		case 'o':
			output = optarg;
			break;
		case 's':
			shift = optarg;
			break;
		case 'k':
			key = optarg;
			break;
		case 'c':
			cflag = true;
			break;
		case 'd':
			dflag = true;
			break;
		case 'v':
			vflag = true;
			break;
		case '?':
			if (optopt == 'c')
				fprintf (stderr, "Option -%c requires an argument.\n", optopt);
			else if (isprint (optopt))
				fprintf (stderr, "Unknown option `-%c'.\n", optopt);
			else
				fprintf (stderr, "Unknown option character `\\x%x'.\n", optopt);
			return 1;
		default:
			abort ();
		}
	if (input == NULL || output == NULL)
		printf("Option -i or -o is missing\n");
	else if (vflag && cflag)
		printf("Choose one of this options:\n-c for Caesar algorithm\n-v for Vigenere algorithm\n");
	else if(cflag)
		if (shift == NULL)
			printf("For the Caesar algorithm you need to enter option -s <shift amount>");
		else if (dflag)
			CaesarDecode(input, output, shift);
		else
			CaesarCode(input, output, shift);
	else if (vflag)
		if (key == NULL)
			printf("For the Vigenere algorithm you need to enter option -k <key_filename>");
		else if (dflag)
			VigenereDecode(input, output, key);
		else
			VigenereCode(input, output, key);
	else
		printf("Choose one of this options:\n-c for Caesar algorithm\n-v for Vigenere algorithm\n");
	return 0;
}

void CaesarCode(char* input, char* output, char* shift)
{
	FILE *fr = fopen(input, "r");
	FILE *fw = fopen(output, "w");
	int tab = atoi(shift);
	tab = tab % 26;
	while (!feof(fr))
	{
		char temp = getc(fr);
		if (temp != EOF)
		{
			if (temp >= 'a' && temp <= 'z')
			{
				if ((int)temp + tab > (int) 'z')
				{
					temp = (int) 'a' - 1 + (tab - ((int) 'z' - (int)temp));
				}
				else
				{
					temp = (int) temp + tab;
				}
				fprintf(fw, "%c", temp);
			}
			if (temp >= 'A' && temp <= 'Z')
				{
				if ((int)temp + tab > (int) 'Z')
				{
					temp = (int) 'A' - 1 + (tab - ((int) 'Z' - (int)temp));
				}
				else
				{
					temp = (int) temp + tab;
				}
				fprintf(fw, "%c", temp);
			}
		}
	}
	fclose(fr);
	fclose(fw);
}

void CaesarDecode(char* input, char* output, char* shift)
{
	FILE *fr = fopen(input, "r");
	FILE *fw = fopen(output, "w");
	int tab = atoi(shift);
	tab = tab % 26;
	while (!feof(fr))
	{
		unsigned char temp = getc(fr);
		if (temp != EOF)
		{
			if (temp >= 'a' && temp <= 'z')
			{
				if ((int)temp - tab < (int) 'a')
				{
					temp = (int) 'z' + 1 - (tab - ((int)temp - (int)'a'));
				}
				else
				{
					temp = (int) temp - tab;
				}
				fprintf(fw, "%c", temp);
			}
			if (temp >= 'A' && temp <= 'Z')
			{
				if ((int)temp - tab < (int) 'A')
				{
					temp = (int) 'Z' + 1 - (tab - ((int)temp - (int)'A'));
				}
				else
				{
					temp = (int) temp - tab;
				}
				fprintf(fw, "%c", temp);
			}
		}
	}
	fclose(fr);
	fclose(fw);
}

void VigenereCode(char* input, char* output, char* key_filename)
{
	FILE *fr = fopen(input, "r");
	FILE *fw = fopen(output, "w");
	FILE *fk = fopen(key_filename, "r");
	fseek(fk, 0, SEEK_END);
	int size = ftell(fk);
	rewind(fk);
	char* key = new char [size + 1];
	fscanf(fk, "%s", key);
	fclose(fk);
	int key_size = strlen(key);
	int counter = -1;
	while (!feof(fr))
	{
		counter++;
		char temp = getc(fr);
		if (temp != EOF)
		{
			int tab;
			if (key[counter % key_size] >= 'a' && key[counter % key_size] <= 'z')
				tab = (int) key[counter % key_size] - (int) 'a';
			if (key[counter % key_size] >= 'A' && key[counter % key_size] <= 'Z')
				tab = (int) key[counter % key_size] - (int) 'A';
			if (temp >= 'a' && temp <= 'z')
			{
				if ((int)temp + tab > (int) 'z')
				{
					temp = (int) 'a' - 1 + (tab - ((int) 'z' - (int)temp));
				}
				else
				{
					temp = (int) temp + tab;
				}
				fprintf(fw, "%c", temp);
			}
			if (temp >= 'A' && temp <= 'Z')
			{
				if ((int)temp + tab > (int) 'Z')
				{
					temp = (int) 'A' - 1 + (tab - ((int) 'Z' - (int)temp));
				}
				else
				{
					temp = (int) temp + tab;
				}
				fprintf(fw, "%c", temp);
			}
		}
	}
	fclose(fr);
	fclose(fw);
	delete [] key;
}

void VigenereDecode(char* input, char* output, char* key_filename)
{
	FILE *fr = fopen(input, "r");
	FILE *fw = fopen(output, "w");
	FILE *fk = fopen(key_filename, "r");
	fseek(fk, 0, SEEK_END);
	int size = ftell(fk);
	rewind(fk);
	char* key = new char [size + 1];
	fscanf(fk, "%s", key);
	fclose(fk);
	int key_size = strlen(key);
	int counter = -1;
	while (!feof(fr))
	{
		counter++;
		char temp = getc(fr);
		if (temp != EOF)
		{
			int tab;
			if (key[counter % key_size] >= 'a' && key[counter % key_size] <= 'z')
				tab = (int) key[counter % key_size] - (int) 'a';
			if (key[counter % key_size] >= 'A' && key[counter % key_size] <= 'Z')
				tab = (int) key[counter % key_size] - (int) 'A';
			if (temp >= 'a' && temp <= 'z')
			{
				if ((int)temp - tab < (int) 'a')
				{
					temp = (int) 'z' + 1 - (tab - ((int)temp - (int)'a'));
				}
				else
				{
					temp = (int) temp - tab;
				}
				fprintf(fw, "%c", temp);
			}
			if (temp >= 'A' && temp <= 'Z')
			{
				if ((int)temp - tab < (int) 'A')
				{
					temp = (int) 'Z' + 1 - (tab - ((int)temp - (int)'A'));
				}
				else
				{
					temp = (int) temp - tab;
				}
				fprintf(fw, "%c", temp);
			}
		}
	}
	fclose(fr);
	fclose(fw);
	delete [] key;
}